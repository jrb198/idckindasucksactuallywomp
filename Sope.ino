#include <Servo.h> 
#include <Wire.h>
#include <SparkFunMLX90614.h>  
#include <HardwareSerial.h>                        // Include servo library
#define lineSensor1 47 
#define lineSensor2 51
#define lineSensor3 52
#define redpin 45
#define bluepin 44
#define greenpin 46
#define TxPin 19
 int red = -1;
    int black = -1;
    int blue = -1;
    int green = -1;
    int yellow = -1;
    char receiving;
bool iscold = 0;
Servo servoLeft;                             // Declare left servo signal
Servo servoRight;  
IRTherm therm;
int score = 0;
int durs[54] = { 211, 209, 213, 210, 212, 212, 212, 212, 211, 211, 211, 211, 211, 210, 211, 209, 211, 211, 210, 211, 209, 211, 211, 210, 211, 210, 211, 211, 211,
                 211, 209, 213, 210, 212, 212, 212, 212, 211, 211, 211, 211, 211, 210, 211, 209, 211, 211, 211, 211, 211, 211, 211, 211, 211 };
int octs[54] = { 217, 217, 217, 217, 217, 217, 217, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 217, 216,
                 217, 217, 217, 217, 217, 217, 217, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216 };
int note[54] = { 224, 232, 222, 232, 232, 222, 221, 231, 232, 231, 231, 229, 229, 224, 227, 232, 224, 227, 229, 231, 232, 229, 229, 227, 229, 232, 231, 222, 231,
                 224, 232, 222, 232, 232, 222, 221, 231, 232, 231, 231, 229, 229, 224, 227, 232, 224, 227, 229, 232, 231, 227, 232, 231, 227 };
  void setup() {
  Serial.begin(9600); //start the serial monitor so we can view the output
  Serial3.begin(9600);
  Serial3.write(12); // clear
  delay(10);
  Serial3.write(22); // no cursor no blink
  delay(10);
  delay(10);
  servoLeft.attach(11);                      // Attach left signal to pin 13
  servoRight.attach(12);  
  Wire.begin();
  Serial2.begin(9600);  // initialize Xbee Tx/Rx
  pinMode(redpin, OUTPUT);
  pinMode(bluepin, OUTPUT);
  pinMode(greenpin, OUTPUT);
  analogWrite(redpin, 255);
  analogWrite(bluepin, 255);
  analogWrite(greenpin, 255);
  if (therm.begin() == 0){
    Serial.println("Qwiic IT Thermometer not acknowledged! Freezing!");
    while(1);
  }          
  Serial.println("Qwiic IR Thermometer acknowledged.");
  therm.setUnit(TEMP_F); 
}

void loop() {
  String middle = "Middle: ";
  String right = "Right: ";
  String left = "Left: ";

int Left = rcTime(lineSensor1); 
 int Middle = rcTime(lineSensor2); 
  int Right = rcTime(lineSensor3); 


  delay(200);
  Serial.print(left);
   Serial.println(Left);  
  
 Serial.print(middle);
  Serial.println(Middle);
  Serial.print(right);
  Serial.println(Right);



bool bLeft = Left<200; 
bool bMiddle = Middle<150; 
bool bRight = Right<200; 
char receiving; 
int state = 4* bLeft + 2*bMiddle + bRight;
  Serial.println(bLeft);  Serial.println(bMiddle); Serial.println(bRight);
  Serial.println(state);

switch (state) {
  case 4:
  case 6:
      RightTurn();
      break;
  case 1 : 
  case 3 :
      LeftTurn();
    break;
  case 7: 
  Back();
  break;
  case 5:
  Straight();
  break;
  case 0:
    Rotate();
    break;

  default:
 Serial.println("Idk");
 servoLeft.writeMicroseconds(1500);         // 1.7 ms -> counterclockwise
    servoRight.writeMicroseconds(1500); 
     break;
}
}

void LeftTurn() {
  servoLeft.writeMicroseconds(1530);  //this should turn left
  servoRight.writeMicroseconds(1450); }
void RightTurn(){ 
  servoLeft.writeMicroseconds(1550);  // this should turn rght 
  servoRight.writeMicroseconds(1470);}
void Straight(){ 
  servoLeft.writeMicroseconds(1550);  
  servoRight.writeMicroseconds(1450);}
  void Back(){ 
  servoLeft.writeMicroseconds(1450);  
  servoRight.writeMicroseconds(1550);}
void Rotate(){ 
  servoLeft.writeMicroseconds(1500);  
  servoRight.writeMicroseconds(1500);
  analogWrite(redpin, 0);
  Serial.println("light on!!");
  delay(1000);
  analogWrite(redpin, 255);
  servoLeft.writeMicroseconds(1450);
  servoRight.writeMicroseconds(1550);
  delay(300);
  servoLeft.writeMicroseconds(1700);  
  servoRight.writeMicroseconds(1700);
  delay(700);
  servoLeft.writeMicroseconds(1700);  
  servoRight.writeMicroseconds(1300);
  delay(2000);
    Serial2.println();       // Send to serial monitor
  servoLeft.writeMicroseconds(1500);  
  servoRight.writeMicroseconds(1500);
  
  Serial.println("calling cold");
  iscold = check_if_cold();
  if (iscold == 1){
    score = 1;
    delay(500);
  }
  delay(1000);
  servoLeft.writeMicroseconds(1700);  
  servoRight.writeMicroseconds(1300);
  delay(1000);
  int iscolds = check_if_cold();
  if (iscolds == 1){
    score = 2;
    }
  while (1 == 1){ 
    Serial.println("Object: " + String(therm.object(), 2));
    Serial.println("Ambient: " + String(therm.ambient(), 2));
    Serial.println(score);
     servoLeft.writeMicroseconds(1500);  
  servoRight.writeMicroseconds(1500);
        if (score == 0){
          Serial2.print("A");
            digitalWrite(bluepin, 0);
            digitalWrite(redpin, 0);
            delay(200);
            digitalWrite(bluepin, 1);
            digitalWrite(redpin, 1);
  }
        if (score == 1){
          Serial2.print("B");
          digitalWrite(bluepin, 0);
            digitalWrite(redpin, 0);
            delay(200);
            digitalWrite(bluepin, 255);
            digitalWrite(redpin, 255);
        }
        if (score == 2){
              Serial2.print("C");
              digitalWrite(bluepin, 0);
            digitalWrite(redpin, 0);
            delay(200);
            digitalWrite(bluepin, 255);
            digitalWrite(redpin, 255);
        }

    char receiving = Serial2.read();
    Serial.println(receiving);
    analogWrite(bluepin, 0);
            analogWrite(greenpin, 0);
            delay(200);
            analogWrite(bluepin, 255);
            analogWrite(greenpin, 255);
  if (receiving == 'A'){
    red = 0;

  } if (receiving == 'B'){
red = 1;

  } if (receiving == 'C'){
red = 2;

  } if(receiving == 'D'){
    black = 0;

  } if(receiving == 'E'){
black = 1;

  } if(receiving == 'F'){
black = 2;

  } if(receiving == 'G'){
blue = 0;

  } if (receiving == 'H'){
blue = 1;

  } if (receiving == 'I'){
blue = 2;

  } if (receiving == 'J'){
green = 0;

  } if (receiving == 'K'){
green = 1;

  } if (receiving == 'L'){
green = 2;

  } if (receiving == 'M'){
yellow = 0;

  } if (receiving == 'N'){
yellow = 1;

  } if (receiving == 'O'){
yellow = 2;
  }
         int receive[5];
         receive[0] = score;
         receive[1] = black;
         receive[2] = blue;
         receive[3] = green;
         receive[4] = yellow;
for (int i = 0; i  < 5; i++){
  Serial.println(receive[i]);
}
         Serial3.print((String)receive[0]);
        Serial3.print((String)receive[1]);
         Serial3.print((String)receive[2]);
         Serial3.print((String)receive[3]);
         Serial3.print((String)receive[4]);
         int mode = receive[0] + receive[1] + receive[2] + receive[3] + receive[4];
         Serial3.write(148);
         int modebinary = mode % 3;
         String binary = "0";
         if (modebinary == 0){
          binary = "00";
         }
         if (modebinary == 1){
          binary = "01";
         }
         if (modebinary == 2){
          binary = "10";
         }
         Serial3.print(binary);
         delay(200);
         Serial3.write(12);
         if ((receiving == 'R')){
         for (int k = 0; k < 55; k++) {
    Serial3.write(durs[k]);
    Serial3.write(octs[k]);
    Serial3.write(note[k]);
    int len = 214 - durs[k];
    float del = 2000 / pow(2, len);
    delay(int(del * 1.1));
  };
  while(1){};}
  if ((receiving == 'P')){
  int i = 0;
  while (i<16){
     servoLeft.writeMicroseconds(1700);  
  servoRight.writeMicroseconds(1700);
  delay(400);
   servoLeft.writeMicroseconds(1300);  
  servoRight.writeMicroseconds(1300);
  delay(800);
  i+=1; 
  }
   servoLeft.writeMicroseconds(1700);  
  servoRight.writeMicroseconds(1700);
  delay(1000);
     servoLeft.writeMicroseconds(1500);  
  servoRight.writeMicroseconds(1500);
  }
  if ((receiving == 'Q')){
    int i = 0;
  while (i < 16){
  analogWrite(redpin, 0);
  analogWrite(greenpin, 255);
  analogWrite(bluepin, 255);
  delay(500);
  analogWrite(redpin, 0);
  analogWrite(greenpin, 0);
  analogWrite(bluepin, 0);
  delay(500);
  analogWrite(redpin, 0);
  analogWrite(greenpin, 0);
  analogWrite(bluepin, 255);
  delay(500);
  analogWrite(redpin, 255);
  analogWrite(greenpin, 0);
  analogWrite(bluepin, 255);
  delay(500);
  analogWrite(redpin, 255);
  analogWrite(greenpin, 255);
  analogWrite(bluepin, 0);
  delay(500);
  analogWrite(redpin, 120);
  analogWrite(greenpin, 0);
  analogWrite(bluepin, 255);
  delay(500);
  analogWrite(redpin, 0);
  analogWrite(greenpin, 255);
  analogWrite(bluepin, 0);
  delay(500);
  i++;
  }
  }
  }
  }
bool check_if_cold(){
  if (therm.read()){
    Serial.print("Object: " + String(therm.object(), 2));
    Serial.println("F");
    Serial.print("Ambient: " + String(therm.ambient(), 2));
    Serial.println();
    if ((therm.object()) < 60.00){
      return 1;
    }
    else{
      return 0;
    }
  }
}
    

long rcTime(int pin)
{
  pinMode(pin, OUTPUT);    // Sets pin as OUTPUT
  digitalWrite(pin, HIGH); // Pin HIGH
  delay(1);                // Waits for 1 millisecond
  pinMode(pin, INPUT);     // Sets pin as INPUT
  digitalWrite(pin, LOW);  // Pin LOW
  long time = micros();    // Tracks starting time
  while(digitalRead(pin)); // Loops while voltage is high
  time = micros() - time;  // Calculate decay time
  return time;             // Return decay time
}